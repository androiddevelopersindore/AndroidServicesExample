package softinator.servicesexampleapp;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;


public class FactorialCalculationService extends IntentService {


    private Handler messageHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Toast.makeText(getApplicationContext(),"Factorial is "+(String)msg.obj,Toast.LENGTH_SHORT).show();
        }
    };
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public FactorialCalculationService()
    {
        super("Factorial Calculation Service");
    }

    public FactorialCalculationService(String name) {
        super(name);

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final long i=intent.getExtras().getLong("factorialOf");

                long e,f;
                e=f=1;
                while(f<=i)
                {
                    e=e*f;
                    f++;
                }
        final long result=e;
        Message message=new Message();
        message.obj=result+"";
        messageHandler.sendMessage(message);
    }

}
